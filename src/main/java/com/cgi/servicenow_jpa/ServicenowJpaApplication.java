package com.cgi.servicenow_jpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * The ServicenowJpaApplication program implements a ticket service system
 * The program has a REST API available at http://localhost:8090/service-now/swagger-ui.html
 * The program can get a ticket from the databases by either id or name, get all the tickets or create a new ticket
 *
 * @author  Martin Kalny
 */
@SpringBootApplication
public class ServicenowJpaApplication {
    public static void main(String[] args) {
        SpringApplication.run(ServicenowJpaApplication.class, args);
    }
}


