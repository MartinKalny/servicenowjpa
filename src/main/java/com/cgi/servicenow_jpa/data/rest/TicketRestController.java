package com.cgi.servicenow_jpa.data.rest;

import com.cgi.servicenow_jpa.data.api.TicketCreateDto;
import com.cgi.servicenow_jpa.data.api.TicketDto;
import com.cgi.servicenow_jpa.data.service.TicketService;
import com.cgi.servicenow_jpa.data.service.mapping.PageResultResource;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * REST controller
 */
@RestController
@RequestMapping(path = "/tickets")
public class TicketRestController {
    private TicketService ticketService;

    @Autowired
    public TicketRestController(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    /**
     * Get requested ticket by id
     * @param id of the requested ticket
     * @return TicketDto
     */
    @GetMapping(path = "/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Get Ticket from ServiceNow by id",
            notes = "Provide an id to find specific ticket",
            response = TicketDto.class)
    public ResponseEntity<TicketDto> getTicketById(@PathVariable Long id) {
        return ResponseEntity.ok(ticketService.findTicketById(id));
    }

    /**
     * Get requested ticket by name
     * @param name of the requested ticket
     * @return TicketDto
     */
    @GetMapping(path = "/name/{name}",produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Get Ticket from ServiceNow by name",
            notes = "Provide a name to find specific ticket",
            response = TicketDto.class)
    public ResponseEntity<TicketDto> getTicketByName(@PathVariable String name) {
        return ResponseEntity.ok(ticketService.findTicketByName(name));
    }

    /**
     * Get all tickets
     * @param pageable is used to specify the form in which the tickets are returned
     * @return PageResultResource<TicketDto>
     */
    @GetMapping
    @ApiOperation(value = "Get all Tickets",
            produces = MediaType.APPLICATION_JSON_VALUE,
            httpMethod = "GET",
            response = TicketDto.class,
            responseContainer = "Page")
    public PageResultResource<TicketDto> getAllTickets(Pageable pageable) {
        return ticketService.findAllTickets(pageable);
    }

    /**
     * This method inserts a ticket to the database
     * @param ticket specifies the information in the ticket
     * @return TicketDto
     */
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Inserts new Ticket to ServiceNow",
            response = TicketDto.class)
    public ResponseEntity<TicketDto> createTicket(
            @ApiParam(value = "TicketCreateDto object", required = true)
            @Valid @RequestBody TicketCreateDto ticket) {
        return ResponseEntity.ok(ticketService.createTicket(ticket));
    }
}
