package com.cgi.servicenow_jpa.data.rest.exceptionhandling;

import com.cgi.servicenow_jpa.data.service.exceptions.ResourceNotCreatedException;
import com.cgi.servicenow_jpa.data.service.exceptions.ResourceNotFoundException;
import org.hibernate.TypeMismatchException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.util.UrlPathHelper;
import javax.servlet.http.HttpServletRequest;

/**
 * This class handles the exceptions to send through the rest controller
 */
@RestControllerAdvice
public class RestExceptionHandler {

    private static final UrlPathHelper URL_PATH_HELPER = new UrlPathHelper();

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<ApiError> resourceNotFoundException(ResourceNotFoundException resourceNotFound,
                                                            final HttpServletRequest request) {
        ApiError apiError = new ApiError(404,
                resourceNotFound.getLocalizedMessage(),
                null,
                URL_PATH_HELPER.getOriginatingServletPath(request));
                apiError.setErrors("Resource not found.");
        return new ResponseEntity<>(apiError, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(ResourceNotCreatedException.class)
    public ResponseEntity<ApiError> resourceNotCreatedException(ResourceNotCreatedException resourceNotCreated,
                                                                final HttpServletRequest request) {
        ApiError apiError = new ApiError(400,
                resourceNotCreated.getLocalizedMessage(),
                null,
                URL_PATH_HELPER.getOriginatingServletPath(request));
        return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(TypeMismatchException.class)
    public ResponseEntity<ApiError> typeMismatchException(TypeMismatchException mismatchException,
                                                          final HttpServletRequest request){
        ApiError apiError = new ApiError(400,
                mismatchException.getLocalizedMessage(),
                null,
                URL_PATH_HELPER.getOriginatingServletPath(request));
                apiError.setErrors("Type mismatch in input argument");
        return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NumberFormatException.class)
    public ResponseEntity<ApiError> numberFormatException(NumberFormatException ex,
                                                          final HttpServletRequest request){
        ApiError apiError = new ApiError(400,
                "Integer was expected, error getting resource " + ex.getLocalizedMessage(),
                null,
                URL_PATH_HELPER.getOriginatingServletPath(request));
                apiError.setErrors("Bad input argument");
        return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
    }

//    @ExceptionHandler(Exception.class)
//    public ResponseEntity<ApiError> handleAll(Exception ex, final HttpServletRequest request) {
//        ApiError apiError = new ApiError(500,
//                ex.getLocalizedMessage(),
//                null,
//                URL_PATH_HELPER.getOriginatingServletPath(request));
//        return new ResponseEntity<>(apiError, HttpStatus.INTERNAL_SERVER_ERROR);
//    }

}
