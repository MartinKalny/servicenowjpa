package com.cgi.servicenow_jpa.data.rest.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * This class configures the swagger documentation
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    /**
     * This method specifies what is documented by swagger
     * @return Docket
     */
    @Bean
    public Docket api(){
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .paths(PathSelectors.ant("/tickets/**"))
                .apis(RequestHandlerSelectors.basePackage("com.cgi.servicenow_jpa.data.rest"))
                .build()
                .apiInfo(apiDetails());
    }

    /**
     * This method provides information about the Swagger documentation
     * @return ApiInfo
     */
    private ApiInfo apiDetails(){
        return new ApiInfo(
                "ServiceNow API",
                "API documentation for ticket service",
                "1.0",
                "Terms of Service",
                "Martin Kalny",
                "Apache 2.0",
                "https://www.apache.org/licenses/"
        );
    }
}
