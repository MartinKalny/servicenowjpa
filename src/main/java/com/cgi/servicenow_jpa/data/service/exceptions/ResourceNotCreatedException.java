package com.cgi.servicenow_jpa.data.service.exceptions;

/**
 * This class is a custom exception used if the resource was not created in the database
 */
public class ResourceNotCreatedException extends RuntimeException{
    public ResourceNotCreatedException() {
    }

    public ResourceNotCreatedException(String message) {
        super(message);
    }

    public ResourceNotCreatedException(String message, Throwable cause) {
        super(message, cause);
    }

    public ResourceNotCreatedException(Throwable cause) {
        super(cause);
    }

    public ResourceNotCreatedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
