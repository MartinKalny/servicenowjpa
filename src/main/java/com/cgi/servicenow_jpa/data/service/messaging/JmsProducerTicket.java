package com.cgi.servicenow_jpa.data.service.messaging;

import com.cgi.servicenow_jpa.data.api.TicketJmsDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import javax.jms.Destination;

/**
 * This class handles the Jms logic
 */
@Service
public class JmsProducerTicket {

    private JmsTemplate jmsTemplate;
    private Destination ticketsQueue;

    @Autowired
    public JmsProducerTicket(JmsTemplate jmsTemplate, Destination ticketsQueue) {
        this.jmsTemplate = jmsTemplate;
        this.ticketsQueue = ticketsQueue;
    }

    /**
     * This method sends information about a newly created ticket to the Jms server
     * @param ticket is the information about the ticket to send
     */
    public void sendTicket(TicketJmsDto ticket){
        jmsTemplate.convertAndSend(ticketsQueue, ticket);
    }


}
