package com.cgi.servicenow_jpa.data.service.config;

import com.cgi.servicenow_jpa.data.api.TicketJmsDto;
import org.apache.activemq.artemis.jms.client.ActiveMQQueue;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;

import javax.jms.Destination;
import java.util.HashMap;
import java.util.Map;

/**
 * This is aconfiguration class for the service layer
 */
@Configuration
public class ServiceConfig {

    /**
     * This method instantiates the ModelMapper
     * @return ModelMapper
     */
    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }
    /**
     * This method instantiates the Jms destination and sets the address of the queue
     * @return Destination
     */
    @Bean
    public Destination ticketsQueue() {
        return new ActiveMQQueue("tickets.queue");
    }

    /**
     * This method instantiates the MappingJackson2MessageConverter
     * @return MappingJackson2MessageConverter
     */
    @Bean
    public MappingJackson2MessageConverter messageConverter() {
        MappingJackson2MessageConverter messageConverter = new MappingJackson2MessageConverter();
        messageConverter.setTypeIdPropertyName("_typeId");
        setTypeMappings(messageConverter);
        return messageConverter;
    }

    /**
     * This method is used to set the TypeMappings for the messageConverter method
     */
    private void setTypeMappings(MappingJackson2MessageConverter messageConverter) {
        Map<String, Class<?>> typeIdMappings = new HashMap<>();
        typeIdMappings.put("ticket", TicketJmsDto.class);
        messageConverter.setTypeIdMappings(typeIdMappings);
    }
}
