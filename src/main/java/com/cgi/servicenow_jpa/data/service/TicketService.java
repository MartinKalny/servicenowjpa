package com.cgi.servicenow_jpa.data.service;

import com.cgi.servicenow_jpa.data.api.TicketCreateDto;
import com.cgi.servicenow_jpa.data.api.TicketDto;
import com.cgi.servicenow_jpa.data.api.TicketJmsDto;
import com.cgi.servicenow_jpa.data.entities.Ticket;
import com.cgi.servicenow_jpa.data.repository.TicketRepository;
import com.cgi.servicenow_jpa.data.service.exceptions.ResourceNotCreatedException;
import com.cgi.servicenow_jpa.data.service.exceptions.ResourceNotFoundException;
import com.cgi.servicenow_jpa.data.service.mapping.BeanMapping;
import com.cgi.servicenow_jpa.data.service.mapping.PageResultResource;
import com.cgi.servicenow_jpa.data.service.messaging.JmsProducerTicket;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.time.LocalDateTime;

/**
 * This class handles the business logic of the program
 */
@Service
@Transactional
public class TicketService {

    private TicketRepository ticketRepository;
    private BeanMapping beanMapping;
    @Autowired
    private JmsProducerTicket jmsProducerTicket;

    @Autowired
    public TicketService(TicketRepository ticketRepository, BeanMapping beanMapping) {
        this.ticketRepository = ticketRepository;
        this.beanMapping = beanMapping;
    }


    /**
     * This method calls the TicketRepository to find ticket by id
     * @param id is the id of the requested
     * @return TicketDto
     */
    public TicketDto findTicketById(Long id) {
        Ticket ticket = ticketRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Resource with id: " + id + " not found."));
        return beanMapping.mapTo(ticket, TicketDto.class);
    }

    /**
     * This method calls the TicketRepository to find ticket by name
     * @param name is the name of the requested
     * @return TicketDto
     */
    public TicketDto findTicketByName(String name) {
        Ticket ticket = ticketRepository.findByName(name)
                .orElseThrow(() -> new ResourceNotFoundException("Resource with name: " + name + " not found."));
        return beanMapping.mapTo(ticket, TicketDto.class);
    }

    /**
     * This method calls the TicketRepository to find all tickets
     * @param pageable specifies the form in which to return the tickets
     * @return PageResultResource<TicketDto>
     */
    public PageResultResource<TicketDto> findAllTickets(Pageable pageable) {
        Page<Ticket> tickets = ticketRepository.findAll(pageable);
        return beanMapping.mapToPageResource(tickets, TicketDto.class);
    }

    /**
     * This method calls the TicketRepository.class to save the ticket input as a parameter
     * and returns the ticket for confirmation
     * @param ticketCreateDto specifies the inserted ticket information
     * @return PageResultResource<TicketDto>
     */
    public TicketDto createTicket(TicketCreateDto ticketCreateDto) {

            Ticket ticket = beanMapping.mapTo(ticketCreateDto, Ticket.class);
            ticket.setCreationDatetime(LocalDateTime.now());
            try {
                Ticket ticketReturned = ticketRepository.saveAndFlush(ticket);
                jmsProducerTicket.sendTicket(beanMapping.mapTo(ticketReturned, TicketJmsDto.class));
                return beanMapping.mapTo(ticketReturned, TicketDto.class);
            }catch (HibernateException ex){
                throw new ResourceNotCreatedException("Resource could not be created", ex);
            }
        }

}
