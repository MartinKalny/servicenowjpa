package com.cgi.servicenow_jpa.data.api;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * This class is used to handle information about a inserted ticket
 */
@ApiModel(value = "TicketCreateDto", description = "Details about ticket to insert to ServiceNow")
public class TicketCreateDto {

    @NotNull
    @NotEmpty
    @ApiModelProperty(value = "Ticket name", example = "name1")
    private String name;
    @ApiModelProperty(value = "Person assigned ID", example = "1")
    private Long idPersonAssigned;
    @ApiModelProperty(value = "Person creator ID", example = "1")
    private Long idPersonCreator;

    public TicketCreateDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getIdPersonAssigned() {
        return idPersonAssigned;
    }

    public void setIdPersonAssigned(Long idPersonAssigned) {
        this.idPersonAssigned = idPersonAssigned;
    }

    public Long getIdPersonCreator() {
        return idPersonCreator;
    }

    public void setIdPersonCreator(Long idPersonCreator) {
        this.idPersonCreator = idPersonCreator;
    }

    @Override
    public String toString() {
        return "TicketCreateDto{" +
                "name='" + name + '\'' +
                ", idPersonAssigned=" + idPersonAssigned +
                ", idPersonCreator=" + idPersonCreator +
                '}';
    }
}
