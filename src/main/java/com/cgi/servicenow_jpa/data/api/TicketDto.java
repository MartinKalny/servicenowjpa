package com.cgi.servicenow_jpa.data.api;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDateTime;

/**
 * This class is used to return information about a requested ticket
 */
@ApiModel(value = "TicketDto", description = "Details about a ticket from ServiceNow")
public class TicketDto {
    @ApiModelProperty(value = "Ticket ID", example = "1")
    private Long id;
    @ApiModelProperty(value = "Ticket name", example = "name1")
    private String name;
    @ApiModelProperty(value = "Person assigned ID", example = "1")
    private Long idPersonAssigned;
    @ApiModelProperty(value = "Person creator ID", example = "1")
    private Long idPersonCreator;
    @ApiModelProperty(value = "Date and Time of ticket creation", example = "2020-02-20T20:20:20")
    private LocalDateTime creationDatetime;

    public TicketDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getIdPersonAssigned() {
        return idPersonAssigned;
    }

    public void setIdPersonAssigned(Long idPersonAssigned) {
        this.idPersonAssigned = idPersonAssigned;
    }

    public Long getIdPersonCreator() {
        return idPersonCreator;
    }

    public void setIdPersonCreator(Long idPersonCreator) {
        this.idPersonCreator = idPersonCreator;
    }

    public LocalDateTime getCreationDatetime() {
        return creationDatetime;
    }

    public void setCreationDatetime(LocalDateTime creationDatetime) {
        this.creationDatetime = creationDatetime;
    }

    @Override
    public String toString() {
        return "TicketDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", idPersonAssigned=" + idPersonAssigned +
                ", idPersonCreator=" + idPersonCreator +
                ", creationDatetime=" + creationDatetime +
                '}';
    }
}
