package com.cgi.servicenow_jpa.data.api;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * This class is used when sending information about a new ticket to the Jms server
 */
public class TicketJmsDto implements Serializable {
    private Long id;
    private String name;

    public TicketJmsDto() {
    }

    public TicketJmsDto(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "TicketJmsDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
