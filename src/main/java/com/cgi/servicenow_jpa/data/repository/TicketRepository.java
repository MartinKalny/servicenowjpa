package com.cgi.servicenow_jpa.data.repository;

import com.cgi.servicenow_jpa.data.entities.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * This class handles the communication with the database
 */
@Repository
public interface TicketRepository extends JpaRepository<Ticket, Long> {

    /**
     * This method finds a ticket in the database by its name
     * @param name is the name of ticket
     * @return Ticket
     */
    @Query("SELECT t FROM Ticket t WHERE t.name = :name")
    Optional<Ticket> findByName(@Param("name") String name);

}
