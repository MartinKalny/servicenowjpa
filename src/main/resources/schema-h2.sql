-- DB for unit tests
CREATE TABLE TICKET(id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(100), id_person_creator INT, id_person_assigned INT,
    creation_datetime TIMESTAMP, ticket_close_datetime TIMESTAMP);

CREATE SEQUENCE ticket_id_seq
 START WITH     1
 INCREMENT BY   1
