package com.cgi.servicenow_jpa.rest;

import com.cgi.servicenow_jpa.data.api.TicketDto;
import com.cgi.servicenow_jpa.data.rest.TicketRestController;
import com.cgi.servicenow_jpa.data.service.TicketService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.internal.matchers.Equals;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.StatusAssertions;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(TicketRestController.class)
public class RestUnitTest {
    @MockBean
    private TicketService ticketService;

    @Autowired
    private MockMvc mvc;

    private TicketDto ticket;

    @Before
    public void init() {
//        MockitoAnnotations.initMocks(this);
//        ticketService = new TicketService(ticketService);

        ticket = new TicketDto();
        ticket.setName("ticket1");
        ticket.setId(1L);
    }

    @Test
    public void getTicketByName() throws Exception {

        given(ticketService.findTicketByName(ticket.getName())).willReturn(ticket);

        MockHttpServletResponse result = this.mvc.perform(MockMvcRequestBuilders.get("/tickets/name/{name}", ticket.getName())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse();

        assertEquals(result.getContentAsString(),convertObjectToJsonBytes(ticket));
    }

    @Test
    public void getTicketById() throws Exception {

        given(ticketService.findTicketById(ticket.getId())).willReturn(ticket);

        MockHttpServletResponse result = this.mvc.perform(MockMvcRequestBuilders.get("/tickets/{id}", ticket.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse();

        assertEquals(result.getContentAsString(),convertObjectToJsonBytes(ticket));
    }

    private static String convertObjectToJsonBytes(Object object) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
        return mapper.writeValueAsString(object);
    }

}
