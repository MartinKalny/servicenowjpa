package com.cgi.servicenow_jpa.service;

import com.cgi.servicenow_jpa.data.api.TicketDto;
import com.cgi.servicenow_jpa.data.entities.Ticket;
import com.cgi.servicenow_jpa.data.repository.TicketRepository;
import com.cgi.servicenow_jpa.data.service.TicketService;
import com.cgi.servicenow_jpa.data.service.mapping.BeanMapping;
import io.swagger.annotations.ApiParam;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;

@RunWith(SpringRunner.class)
public class ServiceUnitTest {
    @Mock
    private TicketRepository ticketRepository;
    @Mock
    private BeanMapping beanMapping;
    private TicketService ticketService;

    private Ticket ticket;
    private TicketDto ticketDto;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        ticketService = new TicketService(ticketRepository, beanMapping);

        ticket = new Ticket();
        ticket.setName("ticket1");
        ticket.setId(2L);
        ticketDto = new TicketDto();
        ticketDto.setName(ticket.getName());
        ticketDto.setId(ticket.getId());
    }

    @Test
    public void getTicketByName() {
        given(ticketRepository.findByName(any(String.class))).willReturn(java.util.Optional.ofNullable(ticket));
        given(beanMapping.mapTo(any(Ticket.class), eq(TicketDto.class))).willReturn(ticketDto);

        TicketDto ticketResponse = ticketService.findTicketByName("ticket1");
        assertEquals(ticketResponse.getName(), ticket.getName());
    }
    @Test
    public void getTicketById() {
        given(ticketRepository.findById(any(Long.class))).willReturn(java.util.Optional.ofNullable(ticket));
        given(beanMapping.mapTo(any(Ticket.class), eq(TicketDto.class))).willReturn(ticketDto);

        TicketDto ticketResponse = ticketService.findTicketById(2L);
        assertEquals(ticketResponse.getName(), ticket.getName());
    }
}
