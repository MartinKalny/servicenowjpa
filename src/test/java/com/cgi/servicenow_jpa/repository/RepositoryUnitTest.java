package com.cgi.servicenow_jpa.repository;

import com.cgi.servicenow_jpa.data.entities.Ticket;
import com.cgi.servicenow_jpa.data.repository.TicketRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase
public class RepositoryUnitTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private TicketRepository ticketRepository;

    @Test
    public void create_findById() {
        Ticket ticket = new Ticket();
        ticket.setName("ticket");
        entityManager.persist(ticket);
        entityManager.flush();

        Ticket found = ticketRepository.findById(ticket.getId()).get();
        assertEquals(found.getName(), ticket.getName());
    }

    @Test
    public void create_findByName() {
        System.out.println("findByName");
        Ticket ticket = new Ticket();
        ticket.setName("ticket2");
        entityManager.persist(ticket);
        entityManager.flush();

        Ticket found = ticketRepository.findByName(ticket.getName()).get();
        assertEquals(found.getName(), ticket.getName());
    }
    @Test
    public void create_findAll() {
        Ticket ticket1 = new Ticket();
        ticket1.setName("ticket1");
        entityManager.persist(ticket1);
        entityManager.flush();
        entityManager.clear();

        Ticket ticket2 = new Ticket();;
        ticket2.setName("ticket2");
        entityManager.persist(ticket2);
        entityManager.flush();

        List<Ticket> found = new ArrayList<>();
        found = ticketRepository.findAll();
        assertEquals(found.get(0).getName(), ticket1.getName());
        assertEquals(found.get(1).getName(), ticket2.getName());
    }
}
